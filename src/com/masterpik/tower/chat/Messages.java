package com.masterpik.tower.chat;

import com.masterpik.messages.spigot.Api;

public class Messages {

    public static String masterpik = Api.getString("general.masterpik.scoreboard.header");

    public static String main = Api.getString("tower.chat.mainPrefix");
    public static String team = Api.getString("general.all.miniGames.chat.teamPrefix");
    public static String global = Api.getString("general.all.miniGames.chat.allPrefix");

    public static String Join = Api.getString("general.all.miniGames.chat.playerJoin");
    public static String Quit = Api.getString("general.all.miniGames.chat.playerQuit");

    public static String teamIsFull1 = Messages.main+Api.getString("general.all.miniGames.chat.teamFull.1");
    public static String teamIsFull2 = Api.getString("general.all.miniGames.chat.teamFull.2");
    public static String joinTeam = Messages.main+Api.getString("general.all.miniGames.chat.teamJoin");

    public static String countRebour = Api.getString("general.all.miniGames.coolDown.partyStarting");
    public static String partyStart = Api.getString("general.all.miniGames.coolDown.partyStart");
    public static String partyStartTitle = Api.getString("general.all.miniGames.title.partyStart");

    public static String youDied = Api.getString("general.all.miniGames.detection.died");

    public static String chooseInventoryTitle = Api.getString("general.all.miniGames.inventoryTitle.teamChoose")+" ";

    public static String playerMark1 = Api.getString("tower.chat.playerMark.1");
    public static String playerMark2 = Api.getString("tower.chat.playerMark.2");
    public static String win1 = Api.getString("general.all.miniGames.detection.teamWin.1");
    public static String win2 = Api.getString("general.all.miniGames.detection.teamWin.2");
    public static String endParty = Messages.main+Api.getString("general.all.miniGames.detection.end");

    public static String bloc1 = "▇";

    public static String startingMessageDescription = Api.getString("tower.tutorial.startGame");

    public static String statsRegister = Api.getString("stats.register");

}
