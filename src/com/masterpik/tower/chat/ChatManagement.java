package com.masterpik.tower.chat;

import com.masterpik.tower.party.PlayerManagement;
import com.masterpik.tower.party.TeamManagement;
import com.masterpik.tower.party.TowerPlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class ChatManagement {

    public static String transformTeam(String message, Player player) {
        String nuew = "";

        if (PlayerManagement.playerToTowerPlayer(player) != null) {

            nuew = "" + Messages.main + "" + Messages.team + "" + TeamManagement.colorToChatString(PlayerManagement.playerToTowerPlayer(player).getTeam().getColor()) + "§l" + player.getName() + "§l ➜§r§o§l " + message + "";
        }
        return nuew;
    }

    public static String transformGlobal(String message, Player player) {
        String nuew = "";

        if (PlayerManagement.playerToTowerPlayer(player) != null) {

            nuew = "" + Messages.main + "" + Messages.global + "" + TeamManagement.colorToChatString(PlayerManagement.playerToTowerPlayer(player).getTeam().getColor()) + "§l" + player.getName() + "§l ➜§r " + message + "";
        }
        return nuew;
    }

    public static void send(String message, Player player) {
        player.sendMessage(message);
    }

    public static void sendToGlobal(String message, Player player) {

        ArrayList<TowerPlayer> players = new ArrayList<>();

        if (PlayerManagement.playerToTowerPlayer(player) != null) {

            players.addAll(PlayerManagement.playerToTowerPlayer(player).getParty().getPlayers().values());

            int bucle = 0;

            while (bucle < players.size()) {
                players.get(bucle).getPlayer().sendMessage(message);
                bucle++;
            }
        }

    }

    public static void sendToTeam(String message, Player player) {

        ArrayList<TowerPlayer> players = new ArrayList<>();

        if (PlayerManagement.playerToTowerPlayer(player) != null) {

            players.addAll(PlayerManagement.playerToTowerPlayer(player).getTeam().getPlayers().values());

            int bucle = 0;
            while (bucle < players.size()) {
                players.get(bucle).getPlayer().sendMessage(message);
                bucle++;
            }
        }
    }

    public static void sendMessage(String message, Player player) {
        if (message.charAt(0) == '@'
                || message.charAt(0) == '!') {
            message = message.substring(1);
            message = transformGlobal(message, player);
            sendToGlobal(message, player);
        } else {
            message = transformTeam(message, player);
            sendToTeam(message, player);
        }
    }

    public static void brodcasteMessage(String message, Player player) {
        message = ""+Messages.main+""+Messages.global+""+message;
        sendToGlobal(message, player);
    }

}
