package com.masterpik.tower.commands;

import com.masterpik.tower.Main;
import com.masterpik.tower.ScoreboardManagement;
import com.masterpik.tower.party.Party;
import com.masterpik.tower.party.PartyManagement;
import com.masterpik.tower.party.PlayerManagement;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;

public class PlayersCommands implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {

            Player player = (Player) sender;

            if (true/*player.getName().equals("axroy") || player.getName().equals("killer_clement") || player.getName().equals("Sultan138")*/) {

                if (command.getName().equalsIgnoreCase("start")) {
                    if (PlayerManagement.playerToTowerPlayer(player) != null) {
                        Party party = PlayerManagement.playerToTowerPlayer(player).getParty();

                        if (!party.isStart()) {

                            party.setForced(true);

                            PartyManagement.startParty(party);
                        }
                        return true;
                    } else {
                        return false;
                    }

                }else if (command.getName().equalsIgnoreCase("spectate")) {
                    Main.spectators.add(player);
                    Bukkit.getPluginManager().callEvent(new PlayerQuitEvent(player, "quit"));
                    player.sendMessage("enter in spectator mode");
                    player.setGameMode(GameMode.SPECTATOR);
                    player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
                    //player.setScoreboard();
                    player.getInventory().clear();
                    ArrayList<Player> players = new ArrayList<>();
                    players.addAll(Bukkit.getOnlinePlayers());

                    int bucle = 0;

                    while (bucle < players.size()) {

                        player.showPlayer(players.get(bucle));
                        players.get(bucle).hidePlayer(player);

                        bucle ++;
                    }

                    return true;
                }

            }

        }

        return false;
    }
}
