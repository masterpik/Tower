package com.masterpik.tower;

import com.masterpik.api.util.UtilBanners;
import com.masterpik.api.util.UtilMaterial;
import com.masterpik.api.util.UtilPlayer;
import com.masterpik.api.util.UtilShield;
import com.masterpik.tower.bungee.BungeeWork;
import com.masterpik.tower.chat.ChatManagement;
import com.masterpik.tower.party.*;
import net.minecraft.server.v1_9_R1.Slot;
import org.bukkit.*;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.craftbukkit.v1_9_R1.block.CraftBanner;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.*;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.Collections;

public class Events implements Listener {

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void PlayerMoveEvent(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer(player);

        if (Tplayer != null) {

            if (player.getLocation().getBlock().getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).getType().equals(Material.GLASS)
                    && player.getLocation().getBlock().getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).getType().equals(Material.STAINED_GLASS)
                    && !player.getGameMode().equals(GameMode.SPECTATOR)) {
                player.setFallDistance(0);
            }

            if (player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().equals(Material.GLASS)
                    && player.getLocation().getBlock().getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).getType().equals(Material.STAINED_GLASS)
                    && !player.getGameMode().equals(GameMode.SPECTATOR)) {
                PartyManagement.playerMark(player);
            }

            if (player.getLocation().getBlockY() <= Settings.minY
                    && !player.isOnGround()
                    && !player.isOp()
                    && !player.getGameMode().equals(GameMode.SPECTATOR)) {

                PlayerManagement.playerDied(player, EntityDamageEvent.DamageCause.VOID, player.getLocation().clone(), "");

            }

            if (Tplayer.isFreeze()
                    && (!player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().equals(Material.FENCE) && !player.getLocation().getBlock().getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).getType().equals(Material.FENCE))) {
                Location tp = Tplayer.getTeam().getRespawnLocation().clone();
                tp.setYaw(player.getLocation().getYaw());
                tp.setPitch(player.getLocation().getPitch());

                player.teleport(tp);

                player.setFoodLevel(20);
                player.setHealth(20);
            }

            Block block = player.getLocation().getBlock().getRelative(BlockFace.DOWN);
            if (block.getType().equals(Material.STAINED_CLAY)
                    && !Tplayer.getPlayer().getGameMode().equals(GameMode.SPECTATOR)
                    && Tplayer.getPlayer().getLocation().getBlock().getType().equals(Material.AIR)) {
                block.setData(Tplayer.getTeam().getColor());
            }

            if (block.getType().equals(Material.FENCE)
                    && block.getRelative(BlockFace.DOWN).getType().equals(Material.STATIONARY_WATER)
                    && !player.getGameMode().equals(GameMode.SPECTATOR)) {
                player.getInventory().addItem(new ItemStack(Material.BAKED_POTATO));
            }
        }


    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void BlockBreakEvent(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();

        if (Settings.cantBreak.contains(block.getType())) {
            event.setCancelled(true);
        }

        if (block.getRelative(BlockFace.UP).getType().equals(Material.CHEST)
                && block.getType().equals(Material.STAINED_CLAY)) {
            event.setCancelled(true);
        }

        if (block.getRelative(BlockFace.DOWN).getType().equals(Material.STATIONARY_WATER)
                && block.getType().equals(Material.FENCE)) {
            event.setCancelled(true);
        }

        if (block.getType().equals(Material.QUARTZ_STAIRS)
                && block.getY() <= 200) {
            event.setCancelled(true);
        }

        if (block.getType().equals(Material.SEA_LANTERN)) {
            event.getBlock().getLocation().getWorld().dropItem(event.getBlock().getLocation(), new ItemStack(Material.SEA_LANTERN));
            /*event.getBlock().getDrops().removeAll(event.getBlock().getDrops());
            event.getBlock().getDrops().add(new ItemStack(Material.SEA_LANTERN));*/
        }

        /*if (block.getRelative(BlockFace.EAST).getType().equals(Material.WALL_BANNER)
                || block.getRelative(BlockFace.NORTH).getType().equals(Material.WALL_BANNER)
                || block.getRelative(BlockFace.SOUTH).getType().equals(Material.WALL_BANNER)
                || block.getRelative(BlockFace.WEST).getType().equals(Material.WALL_BANNER)) {
            event.setCancelled(true);
        }*/

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void ItemSpawnEvent(ItemSpawnEvent event) {

        Material dropped = event.getEntity().getItemStack().getType();

        if (dropped.equals(Material.LEATHER_HELMET)
                || dropped.equals(Material.LEATHER_CHESTPLATE)
                || dropped.equals(Material.LEATHER_LEGGINGS)
                || dropped.equals(Material.LEATHER_BOOTS)
                //|| dropped.equals(Material.BAKED_POTATO)
                || dropped.equals(Material.PRISMARINE)
                || dropped.equals(Material.PRISMARINE_CRYSTALS)
                || dropped.equals(Material.PRISMARINE_SHARD)) {
            event.setCancelled(true);
        }

        /*if (dropped.equals(Material.PRISMARINE)
                || dropped.equals(Material.PRISMARINE_CRYSTALS)
                || dropped.equals(Material.PRISMARINE_SHARD))
            event.getEntity().setItemStack(new ItemStack(Material.SEA_LANTERN));*/

        if (dropped.equals(Material.STAINED_CLAY)) {
            ItemStack item = new ItemStack(Material.STAINED_CLAY, event.getEntity().getItemStack().getAmount());
            event.getEntity().setItemStack(item);
        }

        if (dropped.equals(Material.SHIELD)) {
            event.getEntity().setItemStack(UtilShield.patternOnShield(new ItemStack(Material.SHIELD), DyeColor.WHITE, null));
        }

        if (dropped.equals(Material.QUARTZ_STAIRS)) {
            event.getEntity().setItemStack(new ItemStack(Material.QUARTZ_BLOCK));
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerPickupItemEvent(PlayerPickupItemEvent event) {
        Material dropped = event.getItem().getItemStack().getType();

        if (dropped.equals(Material.SHIELD)) {
            TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer(event.getPlayer());
            if (Tplayer != null) {
                event.getItem().getItemStack().setItemMeta(UtilShield.patternOnShield(new ItemStack(Material.SHIELD), Tplayer.getTeam().getBanners().get(0).getBaseColor(), null).getItemMeta());
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void BlockPlaceEvent(BlockPlaceEvent event) {

        if (event.getBlockPlaced().getLocation().getY() >= 254) {
            event.getBlockPlaced().breakNaturally();
            return;
        }

        TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer(event.getPlayer());

        if (Tplayer != null) {

            Block block = event.getBlock();

            Location location = block.getLocation();

            if (block.getType().equals(Material.STAINED_CLAY)
                    && !Tplayer.getPlayer().getGameMode().equals(GameMode.SPECTATOR)) {
                block.setData(Tplayer.getTeam().getColor());
            }


            Location mooveLoc = location.clone();

            mooveLoc.setY(199);
            Block blockAt199 = mooveLoc.clone().getBlock();

            mooveLoc.setY(195);
            Block blockAt195 = mooveLoc.clone().getBlock();


            if (blockAt199.getType().equals(Material.BEACON)) {
                block.setTypeIdAndData(95, blockAt199.getRelative(BlockFace.UP).getData(), false);
            } else if (blockAt195.getType().equals(Material.BEACON)) {
                block.setTypeIdAndData(95, blockAt195.getRelative(BlockFace.UP).getData(), false);
            }

            Location glassLoc = location.clone();

            glassLoc.setY(201);
            Block blockAt201 = glassLoc.clone().getBlock();
            glassLoc.setY(194);
            Block blockAt194 = glassLoc.clone().getBlock();

            if ((blockAt201.getType().equals(Material.GLASS)
                    || blockAt201.getType().equals(Material.STAINED_GLASS))
                    && ((event.getBlock().getLocation().getBlockY() < 207
                    && event.getBlock().getLocation().getBlockY() > 193) || Settings.cantBreak.contains(block.getType()))
                    && (blockAt195.getType().equals(Material.STAINED_GLASS) || blockAt194.getType().equals(Material.STAINED_GLASS))) {
                event.setCancelled(true);
                if (event.getBlockPlaced().getLocation().getBlockY() != 195) {
                    event.getBlockReplacedState().setType(Material.AIR);
                }
            }

            if (block.getRelative(BlockFace.DOWN).getType().equals(Material.CHEST)
                    && !block.getType().equals(Material.CHEST)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerInteractEvent(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer(player);

        if (Tplayer != null) {

            Team team = Tplayer.getTeam();
            Action action = event.getAction();
            Block block = event.getClickedBlock();

            if (action.equals(Action.RIGHT_CLICK_BLOCK)) {

                if (block.getType().equals(Material.DAYLIGHT_DETECTOR)
                        || block.getType().equals(Material.DAYLIGHT_DETECTOR_INVERTED)) {
                    event.setCancelled(true);
                }

                if (block.getType().equals(Material.CHEST) || block.getType().equals(Material.BREWING_STAND)) {
                    Block blockDown = block.getRelative(BlockFace.DOWN);
                    Block blockDownDown = blockDown.getRelative(BlockFace.DOWN);
                    Block blockDownDownDown = blockDownDown.getRelative(BlockFace.DOWN);

                    if ((blockDown.getType().equals(Material.STAINED_CLAY) || blockDown.getType().equals(Material.STAINED_GLASS))

                            || (blockDown.getType().equals(Material.CHEST) && blockDownDown.getType().equals(Material.STAINED_CLAY))
                            || (blockDown.getType().equals(Material.CHEST) && blockDownDown.getType().equals(Material.STAINED_GLASS))

                            || (blockDownDown.getType().equals(Material.CHEST) && blockDownDownDown.getType().equals(Material.STAINED_CLAY))
                            || (blockDownDown.getType().equals(Material.CHEST) && blockDownDownDown.getType().equals(Material.STAINED_GLASS))) {

                        if (((blockDown.getType().equals(Material.STAINED_GLASS) || blockDown.getType().equals(Material.STAINED_CLAY)) && blockDown.getData() != team.getColor())
                                || ((blockDownDown.getType().equals(Material.STAINED_GLASS) || blockDownDown.getType().equals(Material.STAINED_CLAY)) && blockDownDown.getData() != team.getColor())
                                || ((blockDownDownDown.getType().equals(Material.STAINED_GLASS) || blockDownDownDown.getType().equals(Material.STAINED_CLAY)) && blockDownDownDown.getData() != team.getColor())) {
                            event.setCancelled(true);
                        }

                    }

                }

                ItemStack item = player.getInventory().getItemInMainHand().clone();

                if (item != null && UtilMaterial.isArmor(item.getType())) {
                    if (player.getInventory().getHelmet().getType().equals(Material.LEATHER_HELMET)
                            && UtilMaterial.isHelmet(item.getType())) {
                        player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
                        player.playSound(player.getLocation(), Sound.ITEM_ARMOR_EQUIP_IRON, Settings.volumeSound, Settings.pitchSound);
                        player.getInventory().setHelmet(item);
                    }
                    else if (player.getInventory().getChestplate().getType().equals(Material.LEATHER_CHESTPLATE)
                            && UtilMaterial.isChestplate(item.getType())) {
                        player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
                        player.playSound(player.getLocation(), Sound.ITEM_ARMOR_EQUIP_IRON, Settings.volumeSound, Settings.pitchSound);
                        player.getInventory().setChestplate(item);
                    }
                    else if (player.getInventory().getLeggings().getType().equals(Material.LEATHER_LEGGINGS)
                            && UtilMaterial.isLeggings(item.getType())) {
                        player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
                        player.playSound(player.getLocation(), Sound.ITEM_ARMOR_EQUIP_IRON, Settings.volumeSound, Settings.pitchSound);
                        player.getInventory().setLeggings(item);
                    }
                    else if (player.getInventory().getBoots().getType().equals(Material.LEATHER_BOOTS)
                            && UtilMaterial.isBoots(item.getType())) {
                        player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
                        player.playSound(player.getLocation(), Sound.ITEM_ARMOR_EQUIP_IRON, Settings.volumeSound, Settings.pitchSound);
                        player.getInventory().setBoots(item);
                    }
                }

            }


            if (player.getItemInHand().isSimilar(LobbyItems.lobbyItemHub)) {
                event.setCancelled(true);
                BungeeWork.sendPlayer(player, "hub");
            } else if (player.getItemInHand().getType().equals(Material.BANNER)) {
                event.setCancelled(true);
                PartyManagement.changeTeamPlayer(Tplayer, player.getItemInHand());
            } else if (player.getItemInHand().isSimilar(LobbyItems.teamitem)) {
                event.setCancelled(true);
                player.openInventory(LobbyItems.lobbyInventoryMapTeams.get(Tplayer.getParty().getNbTeam()));
            }

            if (player.getGameMode().equals(GameMode.SPECTATOR)) {
                event.setCancelled(true);
            }
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void InventoryClickEvent(InventoryClickEvent event) {

        Player player = (Player) event.getWhoClicked();

        TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer(player);

        if (Tplayer != null) {

            Inventory inv = event.getInventory();

            ItemStack item = new ItemStack(Material.AIR);

            if (event.getCurrentItem() != null) {

                item = event.getCurrentItem().clone();
            }

            if (item.isSimilar(LobbyItems.lobbyItemHub)) {
                event.setCancelled(true);
                BungeeWork.sendPlayer(player, "hub");
            } else if (item.getType().equals(Material.BANNER)) {
                event.setCancelled(true);
                PartyManagement.changeTeamPlayer(Tplayer, item);
            } else if (item.isSimilar(LobbyItems.teamitem)) {
                event.setCancelled(true);
                BukkitTask time = Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(LobbyItems.lobbyInventoryMapTeams.get((byte) Tplayer.getParty().getNbTeam()));
                    }
                }, 1);
            }

            if (player.getWorld().equals(Bukkit.getWorld("lobby"))) {
                event.setCancelled(true);
            }


            if (event.getSlotType().equals(InventoryType.SlotType.ARMOR)) {
                if (event.getAction().equals(InventoryAction.SWAP_WITH_CURSOR)) {
                    if (UtilMaterial.isArmor(player.getItemOnCursor().getType())) {
                        if (item.getType().equals(Material.LEATHER_HELMET)
                                || item.getType().equals(Material.LEATHER_CHESTPLATE)
                                || item.getType().equals(Material.LEATHER_LEGGINGS)
                                || item.getType().equals(Material.LEATHER_BOOTS)) {
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    player.setItemOnCursor(new ItemStack(Material.AIR));
                                }
                            }, 1);
                            /*if (Bukkit.getPlayer("axroy") != null) {
                                Bukkit.getPlayer("axroy").sendMessage("set air on cursor");
                            }*/
                        }
                    }
                } else {
                    if (item.getType().equals(Material.LEATHER_HELMET)
                            || item.getType().equals(Material.LEATHER_CHESTPLATE)
                            || item.getType().equals(Material.LEATHER_LEGGINGS)
                            || item.getType().equals(Material.LEATHER_BOOTS)) {
                        if (event.getInventory().getItem(event.getSlot()) == null
                                || event.getInventory().getItem(event.getSlot()).getType().equals(Material.AIR)) {
                            event.setCancelled(true);
                            /*if (Bukkit.getPlayer("axroy") != null) {
                                Bukkit.getPlayer("axroy").sendMessage("cancel event");
                            }*/
                        }
                    } else {
                        final ItemStack finalItem = item;
                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                if (finalItem != null) {
                                    if (UtilMaterial.isHelmet(finalItem.getType())
                                            && (player.getInventory().getHelmet() == null || player.getInventory().getHelmet().getType().equals(Material.AIR))) {
                                        player.getInventory().setHelmet(Utils.getHelmet(Tplayer.getTeam().getColor()));
                                        /*if (Bukkit.getPlayer("axroy") != null) {
                                            Bukkit.getPlayer("axroy").sendMessage("set helmet");
                                        }*/
                                    } else if (UtilMaterial.isChestplate(finalItem.getType())
                                            && (player.getInventory().getChestplate() == null || player.getInventory().getChestplate().getType().equals(Material.AIR))) {
                                        player.getInventory().setChestplate(Utils.getChestplate(Tplayer.getTeam().getColor()));
                                        /*if (Bukkit.getPlayer("axroy") != null) {
                                            Bukkit.getPlayer("axroy").sendMessage("set chestplate");
                                        }*/
                                    } else if (UtilMaterial.isLeggings(finalItem.getType())
                                            && (player.getInventory().getLeggings() == null || player.getInventory().getLeggings().getType().equals(Material.AIR))) {
                                        player.getInventory().setLeggings(Utils.getLeggings(Tplayer.getTeam().getColor()));
                                        /*if (Bukkit.getPlayer("axroy") != null) {
                                            Bukkit.getPlayer("axroy").sendMessage("set leg");
                                        }*/
                                    } else if (UtilMaterial.isBoots(finalItem.getType())
                                            && (player.getInventory().getBoots() == null || player.getInventory().getBoots().getType().equals(Material.AIR))) {
                                        player.getInventory().setBoots(Utils.getBoots(Tplayer.getTeam().getColor()));
                                        /*if (Bukkit.getPlayer("axroy") != null) {
                                            Bukkit.getPlayer("axroy").sendMessage("set boots");
                                        }*/
                                    }
                                }
                            }
                        }, 1);
                    }
                }
            } else if (event.isShiftClick()) {

                if (event.getClick().equals(ClickType.SHIFT_LEFT)) {
                    if (UtilMaterial.isArmor(item.getType())
                            && !event.getSlotType().equals(InventoryType.SlotType.RESULT)) {

                        if (player.getInventory().getHelmet().getType().equals(Material.LEATHER_HELMET)
                            && UtilMaterial.isHelmet(item.getType())) {
                            event.setCurrentItem(new ItemStack(Material.AIR));
                            player.getInventory().setHelmet(item);
                            //player.getInventory().setHelmet(new ItemStack(Material.AIR));
                        }
                        else if (player.getInventory().getChestplate().getType().equals(Material.LEATHER_CHESTPLATE)
                            && UtilMaterial.isChestplate(item.getType())) {
                            event.setCurrentItem(new ItemStack(Material.AIR));
                            player.getInventory().setChestplate(item);
                            //player.getInventory().setChestplate(new ItemStack(Material.AIR));
                        }
                        else if (player.getInventory().getLeggings().getType().equals(Material.LEATHER_LEGGINGS)
                                && UtilMaterial.isLeggings(item.getType())) {
                            event.setCurrentItem(new ItemStack(Material.AIR));
                            player.getInventory().setLeggings(item);
                            //player.getInventory().setLeggings(new ItemStack(Material.AIR));
                        }
                        else if (player.getInventory().getBoots().getType().equals(Material.LEATHER_BOOTS)
                            && UtilMaterial.isBoots(item.getType())) {
                            event.setCurrentItem(new ItemStack(Material.AIR));
                            player.getInventory().setBoots(item);
                            //player.getInventory().setBoots(new ItemStack(Material.AIR));
                        }
                    }
                }

            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerDropItemEvent(PlayerDropItemEvent event) {

        Item dropped = event.getItemDrop();

        if (event.getPlayer().getWorld().equals(Bukkit.getWorld("lobby"))) {
            event.setCancelled(true);
        }

        if (dropped.getItemStack().getType().equals(Material.LEATHER_HELMET)
                || dropped.getItemStack().getType().equals(Material.LEATHER_CHESTPLATE)
                || dropped.getItemStack().getType().equals(Material.LEATHER_LEGGINGS)
                || dropped.getItemStack().getType().equals(Material.LEATHER_BOOTS)
                //|| dropped.getItemStack().getType().equals(Material.BAKED_POTATO)
                ) {
            event.setCancelled(true);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerJoinEvent(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        event.setJoinMessage(null);

        if (PlayerManagement.isRegister(player)) {
            PlayerManagement.playerJoin(player);
        }
        else {
            player.teleport(Settings.lobbyLocation);

            ArrayList<Integer> waiters = new ArrayList<Integer>();
            waiters.add(Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    if (PlayerManagement.isRegister(player)) {

                        PlayerManagement.playerJoin(player);

                        Bukkit.getScheduler().cancelTask(waiters.get(0));

                    } else {
                        player.teleport(Settings.lobbyLocation);
                    }
                }
            }, 0, 1));
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerQuitEvent(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        event.setQuitMessage(null);
        PlayerManagement.playerQuit(player);


    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerDeathEvent(PlayerDeathEvent event) {

        event.getDrops().removeIf(p -> p.getType().equals(Material.BAKED_POTATO));

        Player player = event.getEntity();
        EntityDamageEvent.DamageCause cause = player.getLastDamageCause().getCause();
        Location diedLocation = player.getLocation().clone();

        String killer = "";

        if (player.getKiller() != null) {
            killer = player.getKiller().getName();
        } else {
            killer = "";
        }
        PlayerManagement.playerDied(player, cause, diedLocation, killer);

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void FoodLevelChangeEvent(FoodLevelChangeEvent event) {
        if (event.getEntity().getWorld().equals(Bukkit.getWorld("lobby"))) {
            event.setCancelled(true);
            ((Player)event.getEntity()).setFoodLevel(20);
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void EntityDamageEvent(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            if (event.getEntity().getWorld().equals(Bukkit.getWorld("lobby"))) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void EntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer((Player) event.getEntity());
            if (Tplayer != null
                    && Tplayer.isRespawnProt()) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void AsyncPlayerChatEvent(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        String message = event.getMessage();
        event.setCancelled(true);

        if (PlayerManagement.isRegister(player)) {

            //TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer(player);

            if (player.getWorld().equals(Bukkit.getWorld("lobby"))) {
                if (message.charAt(0) != '@') {
                    message = "@" + message;
                }
            }

            ChatManagement.sendMessage(message, player);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void WeatherChangeEvent(WeatherChangeEvent event) {
        if (event.toWeatherState()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void BlockFromToEvent(BlockFromToEvent event) {
        Block block = event.getBlock();

        if (block.getType().equals(Material.WATER)
                || block.getType().equals(Material.STATIONARY_WATER)
                || block.getType().equals(Material.LAVA)
                || block.getType().equals(Material.STATIONARY_LAVA)) {
            if (block.getLocation().getBlockY() <= Settings.minY) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void CraftItemEvent(CraftItemEvent event) {

        ItemStack result = event.getRecipe().getResult();

        if (Settings.cantCraft.contains(event.getRecipe().getResult().getType())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PrepareItemCraftEvent(PrepareItemCraftEvent event) {
        ItemStack result = event.getRecipe().getResult();

        if (result != null) {
            if (Settings.cantCraft.contains(result.getType())) {
                event.getInventory().setResult(null);
            } else if (result.getType().equals(Material.SHIELD)) {
                if (!event.getViewers().isEmpty()) {
                    TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer((Player) event.getViewers().get(0));
                    if (Tplayer != null) {
                        event.getInventory().setResult(UtilShield.patternOnShield(new ItemStack(Material.SHIELD), Tplayer.getTeam().getBanners().get(0).getBaseColor(), null));
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void VehicleDestroyEvent(VehicleDestroyEvent event) {
        if (event.getVehicle().getType().equals(EntityType.BOAT)) {
            event.getVehicle().getLocation().getWorld().dropItem(event.getVehicle().getLocation(), new ItemStack(Material.BOAT));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerTeleportEvent(PlayerTeleportEvent event) {
        if (!event.getCause().equals(PlayerTeleportEvent.TeleportCause.PLUGIN)) {
            TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer(event.getPlayer());
            if (Tplayer != null) {
                if (Tplayer.isFreeze()) {
                    if (event.getCause().equals(PlayerTeleportEvent.TeleportCause.SPECTATE)) {
                        event.setCancelled(true);
                    } else {
                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                if (Tplayer.getPlayer().getSpectatorTarget() != null) {
                                    Tplayer.getPlayer().setSpectatorTarget(null);
                                    Location tp = Tplayer.getTeam().getRespawnLocation().clone();
                                    tp.setYaw(Tplayer.getPlayer().getLocation().getYaw());
                                    tp.setPitch(Tplayer.getPlayer().getLocation().getPitch());
                                    Tplayer.getPlayer().teleport(tp);
                                }
                            }
                        }, 1);

                    }
                }
            }
        }
    }

}
