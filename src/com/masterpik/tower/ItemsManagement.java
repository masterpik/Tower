package com.masterpik.tower;

import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemsManagement {

    public static ItemStack helmet;
    public static NbtFactory.NbtCompound helmetComp;

    public static ItemStack chestplate;
    public static NbtFactory.NbtCompound chestplateComp;


    public static ItemStack legg;
    public static NbtFactory.NbtCompound leggComp;

    public static ItemStack boot;
    public static NbtFactory.NbtCompound bootComp;

    public static void ItemsInit() {
        ItemsManagement.helmet = NbtFactory.getCraftItemStack(new ItemStack(Material.LEATHER_HELMET));
        ItemsManagement.helmetComp = NbtFactory.fromItemTag(ItemsManagement.helmet);
        ItemsManagement.helmetComp.putPath("display.Name", "§c§l►§r§c§lcasque§r§c§l◄");
        ItemsManagement.helmetComp.putPath("Unbreakable.", 1);
        ItemMeta helmetM = ItemsManagement.helmet.getItemMeta();
        helmetM.addItemFlags(ItemFlag.HIDE_DESTROYS);
        helmetM.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        ItemsManagement.helmet.setItemMeta(helmetM);

        ItemsManagement.chestplate = NbtFactory.getCraftItemStack(new ItemStack(Material.LEATHER_CHESTPLATE));
        ItemsManagement.chestplateComp = NbtFactory.fromItemTag(ItemsManagement.chestplate);
        ItemsManagement.chestplateComp.putPath("display.Name", "§c§l►§r§c§lplastron§r§c§l◄");
        ItemsManagement.chestplateComp.putPath("Unbreakable.", 1);
        ItemMeta chetplateM = ItemsManagement.chestplate.getItemMeta();
        chetplateM.addItemFlags(ItemFlag.HIDE_DESTROYS);
        chetplateM.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        ItemsManagement.chestplate.setItemMeta(chetplateM);

        ItemsManagement.legg = NbtFactory.getCraftItemStack(new ItemStack(Material.LEATHER_LEGGINGS));
        ItemsManagement.leggComp = NbtFactory.fromItemTag(ItemsManagement.legg);
        ItemsManagement.leggComp.putPath("display.Name", "§c§l►§r§c§lpantalon§r§c§l◄");
        ItemsManagement.leggComp.putPath("Unbreakable.", 1);
        ItemMeta leggM = ItemsManagement.legg.getItemMeta();
        leggM.addItemFlags(ItemFlag.HIDE_DESTROYS);
        leggM.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        ItemsManagement.legg.setItemMeta(leggM);

        ItemsManagement.boot = NbtFactory.getCraftItemStack(new ItemStack(Material.LEATHER_BOOTS));
        ItemsManagement.bootComp = NbtFactory.fromItemTag(ItemsManagement.boot);
        ItemsManagement.bootComp.putPath("display.Name", "§c§l►§r§c§lchaussures§r§c§l◄");
        ItemsManagement.bootComp.putPath("Unbreakable.", 1);
        ItemMeta bootM = ItemsManagement.boot.getItemMeta();
        bootM.addItemFlags(ItemFlag.HIDE_DESTROYS);
        bootM.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        ItemsManagement.boot.setItemMeta(bootM);
    }

}
