package com.masterpik.tower;

import com.masterpik.tower.chat.Messages;
import com.masterpik.tower.party.TeamManagement;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;

public class LobbyItems {

    public static ItemStack lobbyItemHub;
    public static ItemStack teamitem;
    public static HashMap<Byte, Inventory> lobbyInventoryMapTeams;
    public static HashMap<Byte, ItemStack> lobbyItemMapTeams;

    public static void initLobbyItem() {
        lobbyItemHub = new ItemStack(Material.IRON_DOOR, 1);
        ItemMeta lobbyItemHubMeta = lobbyItemHub.getItemMeta();
        lobbyItemHubMeta.setDisplayName("§6§l►§r§6§lHUB§r§6§l◄");
        lobbyItemHub.setItemMeta(lobbyItemHubMeta);

        teamitem = new ItemStack(Material.NETHER_STAR);
        ItemMeta teamitemmeta = teamitem.getItemMeta();
        teamitemmeta.setDisplayName(Messages.chooseInventoryTitle);
        teamitem.setItemMeta(teamitemmeta);

        lobbyItemMapTeams = new HashMap<Byte, ItemStack>();
        lobbyInventoryMapTeams = new HashMap<>();

        int bucle1 = 0;
        ArrayList<Byte> teamsList = new ArrayList<>();
        teamsList.add((byte)0);
        teamsList.add((byte)15);
        teamsList.add((byte)14);
        teamsList.add((byte)5);
        teamsList.add((byte)4);
        teamsList.add((byte)11);

        while (bucle1 < teamsList.size()) {

            lobbyItemMapTeams.put(teamsList.get(bucle1), new ItemStack(Material.BANNER, 1, (short) 0, TeamManagement.byteToBannerByte(teamsList.get(bucle1))));
            ItemMeta meta = lobbyItemMapTeams.get(teamsList.get(bucle1)).getItemMeta();
            meta.setDisplayName(TeamManagement.colorToChatString(teamsList.get(bucle1)) + "§l" + (TeamManagement.colorToString(teamsList.get(bucle1)).toUpperCase()));
            lobbyItemMapTeams.get(teamsList.get(bucle1)).setItemMeta(meta.clone());

            bucle1++;
        }

        int bucle2 = 1;
        int nbB = 0;
        while (bucle2 <= 2) {

            if (bucle2 == 1) {
                nbB = 2;
            } else if (bucle2 == 2) {
                nbB = 4;
            }

            lobbyInventoryMapTeams.put((byte)nbB, Bukkit.createInventory(null, 9, Messages.chooseInventoryTitle));

            LobbyItems.giveLobbyItems((byte) nbB, lobbyInventoryMapTeams.get((byte)nbB));

            bucle2++;
        }



    }

    public static void giveLobbyItems(Byte tip, Inventory inventory) {

        int bucle = 0;

        ArrayList<Byte> teamsList = new ArrayList<>();

        if (tip == 2) {
            teamsList.add((byte)14); //TODO change color
            teamsList.add((byte)11); //TODO change color
        } else if (tip == 4) {
            teamsList.add((byte)14);
            teamsList.add((byte)5);
            teamsList.add((byte)4);
            teamsList.add((byte)11);
        }


        while (bucle < teamsList.size()) {

            inventory.setItem(bucle, lobbyItemMapTeams.get(teamsList.get(bucle)));

            bucle++;
        }

    }

    public static void giveLobbyItems(Player player) {
        player.getInventory().setItem(8, lobbyItemHub);

        player.getInventory().setItem(0, teamitem);

    }

}
