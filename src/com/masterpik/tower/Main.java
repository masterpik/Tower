package com.masterpik.tower;

import com.masterpik.api.util.UtilWorld;
import com.masterpik.games.settings.LobbySettings;
import com.masterpik.tower.bungee.BungeeUtils;
import com.masterpik.tower.bungee.BungeeWork;
import com.masterpik.tower.commands.PlayersCommands;
import com.masterpik.tower.party.PartyManagement;
import com.masterpik.tower.party.PlayerManagement;
import com.masterpik.tower.party.Worlds;
import com.masterpik.tower.spawners.ItemSpawnerManagement;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.ScoreboardManager;

import java.util.ArrayList;

public class Main extends JavaPlugin implements Runnable {

    public static Plugin plugin;
    public static BungeeUtils bungee;
    public static ScoreboardManager scoreboard;

    public static ArrayList<Player> spectators;

    public void onEnable() {
        plugin = this;
        scoreboard = Bukkit.getScoreboardManager();

        Bukkit.getPluginManager().registerEvents(new Events(), this);
        getCommand("start").setExecutor(new PlayersCommands());
        getCommand("spectate").setExecutor(new PlayersCommands());

        Main.bungee = new BungeeUtils(this, "gamesCo");
        Bukkit.getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new BungeeWork(this));

        UtilWorld.loadWorld("lobby");
        PartyManagement.initGamerule(Bukkit.getWorld("lobby"));

        Settings.settingsInit();
        PartyManagement.PartyInit();
        PlayerManagement.PlayersInit();
        LobbyItems.initLobbyItem();
        ItemsManagement.ItemsInit();
        ItemSpawnerManagement.ItemSpawnerIinit();

        ItemSpawnerManagement.startTimer();

        Main.spectators = new ArrayList<>();

        //Bukkit.getScheduler().scheduleSyncDelayedTask(this, this, 10L);

        LobbySettings.setLobbyWorld(Settings.lobbyLocation.getWorld());
        LobbySettings.setCanSwapItem(false);
    }

    public void onDisable() {

    }

    @Override
    public void run() {
        //Worlds.worldInit();
    }

}
