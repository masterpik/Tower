package com.masterpik.tower;

import com.comphenix.protocol.wrappers.EnumWrappers;
import com.masterpik.api.json.ChatText;
import com.masterpik.api.nms.player.Title;
import com.masterpik.api.util.UtilArrayList;
import com.masterpik.api.util.UtilChat;
import com.masterpik.api.util.UtilString;
import com.masterpik.api.util.UtilTitle;
import com.masterpik.tower.party.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.ArrayList;

public class Utils {

    public static String ArrayToString(ArrayList<String> list) {
        /*String string = "";

        int bucle = 0;

        while (bucle < list.size()) {

            if (bucle == 0) {
                string = ""+bucle+":("+list.get(bucle)+")";
            }
            else {
                string = ""+string+""+bucle+":("+list.get(bucle)+")";
            }

            bucle++;
        }


        return string;*/
        return UtilArrayList.ArrayListToCryptedString(list);
    }

    public static ArrayList<String> StringToArray(String string) {
        /*ArrayList<String> list = new ArrayList<String>();

        int bucle1 = 0;
        int bucle2 = 0;
        int count = 0;
        int bcl = 0;
        boolean okay1 = false;
        boolean finish = false;
        int calculsavant = 0;

        bcl = 0;
        while (!finish) {

            //Bukkit.getLogger().info("buclefinish, bcl : "+bcl);

            if (string.length() > bcl) {

                //Bukkit.getLogger().info("firstCond : "+string.charAt(bcl));
                //Bukkit.getLogger().info("secondCon : "+Integer.toString(count).charAt(0));

                if (string.charAt(bcl) == Integer.toString(count).charAt(0)
                        && string.charAt((bcl + 1)) == ':'
                        && string.charAt((bcl + 2)) == '(') {

                    bucle1 = bcl + 3;

                    okay1 = false;
                    while (!okay1) {

                        //Bukkit.getLogger().info("bucle1 while : "+bucle1);

                        if (string.charAt(bucle1) == ')') {
                            String fin = "";

                            calculsavant = 0;
                            calculsavant = (bucle1 - ((bcl + 3)));

                            bucle1 = bucle1 - calculsavant;

                            bucle2 = 0;

                            while (bucle2 < calculsavant) {

                                //Bukkit.getLogger().info("resultOfCalculSavant : "+calculsavant);

                                //Bukkit.getLogger().info("CharAt : "+(bucle1+bucle2));
                                fin = "" + fin + "" + string.charAt(bucle1 + bucle2);

                                bucle2++;
                            }
                            //Bukkit.getLogger().info("bucle2-1 : "+(bucle2));

                            list.add(fin);
                            //Bukkit.getLogger().info("La final : "+ fin);
                            okay1 = true;
                        }

                        bucle1++;
                    }
                    //Bukkit.getLogger().info("bucle1-1 : "+(bucle1-1));

                }
            }
            else {
                finish = true;
            }
            bcl = bcl+4+(bucle2);
            count++;
        }

        return list;*/
        return UtilString.CryptedStringToArrayList(string);
    }

    public static String BytesToPartySource(byte nbTeam, byte nbPlayer) {
        return ""+Byte.toString(nbTeam)+"-"+Byte.toString(nbPlayer)+"";
    }

    public static String BytesToPartyName(byte nbTeam, byte nbPlayer, int nb) {
        return ""+Byte.toString(nbTeam)+"-"+Byte.toString(nbPlayer)+"_"+Integer.toString(nb)+"";
    }

    public static void showPlayersPlayer(Player player) {

        ArrayList<Player> allPlayers = new ArrayList<Player>();
        allPlayers.addAll(Bukkit.getOnlinePlayers());

        int bucle1 = 0;

        while (bucle1 < allPlayers.size()) {
            player.hidePlayer(allPlayers.get(bucle1));
            allPlayers.get(bucle1).hidePlayer(player);
            bucle1 ++;
        }

        ArrayList<TowerPlayer> partyPlayers = new ArrayList<>();

        if (PlayerManagement.playerToTowerPlayer(player) != null) {

            partyPlayers.addAll(PlayerManagement.playerToTowerPlayer(player).getParty().getPlayers().values());

            int bucle2 = 0;

            while (bucle2 < partyPlayers.size()) {

                player.showPlayer(partyPlayers.get(bucle2).getPlayer());
                partyPlayers.get(bucle2).getPlayer().showPlayer(player);

                bucle2++;
            }

        }
        //player.getWorld().refreshChunk(player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ());
        //player.getWorld().loadChunk(player.getLocation().getChunk());

    }

    public static ItemStack getHelmet(Byte color) {
        ItemStack helmet = ItemsManagement.helmet.clone();
        LeatherArmorMeta helmetMeta = (LeatherArmorMeta) helmet.getItemMeta();
        helmetMeta.setColor(TeamManagement.colorToColor(color));
        helmet.setItemMeta(helmetMeta);
        return helmet;
    }

    public static ItemStack getChestplate(Byte color) {
        ItemStack chestplate = ItemsManagement.chestplate.clone();
        LeatherArmorMeta chestplateMeta = (LeatherArmorMeta) chestplate.getItemMeta();
        chestplateMeta.setColor(TeamManagement.colorToColor(color));
        chestplate.setItemMeta(chestplateMeta);
        return chestplate;
    }

    public static ItemStack getLeggings(Byte color) {
        ItemStack legg = ItemsManagement.legg.clone();
        LeatherArmorMeta leggMeta = (LeatherArmorMeta) legg.getItemMeta();
        leggMeta.setColor(TeamManagement.colorToColor(color));
        legg.setItemMeta(leggMeta);
        return legg;
    }

    public static ItemStack getBoots(Byte color) {
        ItemStack boot = ItemsManagement.boot.clone();
        LeatherArmorMeta bootMeta = (LeatherArmorMeta) boot.getItemMeta();
        bootMeta.setColor(TeamManagement.colorToColor(color));
        boot.setItemMeta(bootMeta);
        return boot;
    }

    public static void setArmor(Player player) {

        if (PlayerManagement.playerToTowerPlayer(player) != null) {

            Byte color = PlayerManagement.playerToTowerPlayer(player).getTeam().getColor();

            ItemStack helmet = getHelmet(color);
            /*ItemStack helmet = ItemsManagement.helmet.clone();
            LeatherArmorMeta helmetMeta = (LeatherArmorMeta) helmet.getItemMeta();
            helmetMeta.setColor(TeamManagement.colorToColor(color));
            helmet.setItemMeta(helmetMeta);*/

            ItemStack chestplate = getChestplate(color);
            /*ItemStack chestplate = ItemsManagement.chestplate.clone();
            LeatherArmorMeta chestplateMeta = (LeatherArmorMeta) chestplate.getItemMeta();
            chestplateMeta.setColor(TeamManagement.colorToColor(color));
            chestplate.setItemMeta(chestplateMeta);*/

            ItemStack legg = getLeggings(color);
            /*ItemStack legg = ItemsManagement.legg.clone();
            LeatherArmorMeta leggMeta = (LeatherArmorMeta) legg.getItemMeta();
            leggMeta.setColor(TeamManagement.colorToColor(color));
            legg.setItemMeta(leggMeta);*/

            ItemStack boot = getBoots(color);
            /*ItemStack boot = ItemsManagement.boot.clone();
            LeatherArmorMeta bootMeta = (LeatherArmorMeta) boot.getItemMeta();
            bootMeta.setColor(TeamManagement.colorToColor(color));
            boot.setItemMeta(bootMeta);*/

            player.getInventory().setHelmet(helmet);
            player.getInventory().setChestplate(chestplate);
            player.getInventory().setLeggings(legg);
            player.getInventory().setBoots(boot);
        }

    }

    public static void setAllPartyXp(Party party, int nb) {
        ArrayList<TowerPlayer> players = new ArrayList<>();
        players.addAll(party.getPlayers().values());
        int bucle = 0;
        while (bucle < players.size()) {
            players.get(bucle).getPlayer().setLevel(nb);
            bucle++;
        }
    }

    public static void setAllPartyXp(Player player, int nb) {
        player.setLevel(nb);
    }

    public static void sendSecondTitle(Party party, String sc) {
        ArrayList<TowerPlayer> players = new ArrayList<>();
        players.addAll(party.getPlayers().values());

        int bucle = 0;
        while (bucle < players.size()) {
            //TitleAPI.sendTitle(players.get(bucle).getPlayer(), 0, 20, 0, "§2§l" + sc, "");
            UtilTitle.sendTitle(new Title(EnumWrappers.TitleAction.TITLE, new ChatText("§2§l" + sc), 0, 20, 0), players.get(bucle).getPlayer());
            bucle++;
        }
    }
    public static void sendSecondTitle(Player player, String sc) {

        UtilTitle.sendTitle(new Title(EnumWrappers.TitleAction.TITLE, new ChatText("§2§l" + sc), 0, 20, 0), player);

        //TitleAPI.sendTitle(player, 0, 20, 0, "§2§l" + sc, "");
    }

    public static void actionBarSecond(Party party, int sc) {
        String action[] = {"§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇"};

        switch (sc) {
            case 10 :
                action[0] = "§2▇";
                break;

            case 9 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                break;

            case 8 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                break;

            case 7 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                break;

            case 6 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                break;

            case 5 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                break;

            case 4 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                break;

            case 3 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                action[7] = "§2▇";
                break;

            case 2 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                action[7] = "§2▇";
                action[8] = "§2▇";
                break;

            case 1 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                action[7] = "§2▇";
                action[8] = "§2▇";
                action[9] = "§2▇";
                break;
            case -1 :
                action[0] = "";
                action[1] = "";
                action[2] = "";
                action[3] = "";
                action[4] = "";
                action[5] = "";
                action[6] = "";
                action[7] = "";
                action[8] = "";
                action[9] = "";
                break;
            default:
                action = action;
        }

        String msg = "";

        int bucle1 = 0;
        while (bucle1 < action.length) {
            msg = msg+action[bucle1];
            bucle1++;
        }

        ArrayList<TowerPlayer> players = new ArrayList<>();
        players.addAll(party.getPlayers().values());
        int bucle = 0;
        while (bucle < players.size()) {
            //Action.playAction(players.get(bucle).getPlayer(), msg);
            UtilChat.sendActionBar(new ChatText(msg), players.get(bucle).getPlayer());
            bucle++;
        }
    }
    public static void actionBarSecond(Player player, int sc) {
        String action[] = {"§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇"};

        switch (sc) {
            case 10 :
                action[0] = "§2▇";
                break;

            case 9 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                break;

            case 8 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                break;

            case 7 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                break;

            case 6 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                break;

            case 5 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                break;

            case 4 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                break;

            case 3 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                action[7] = "§2▇";
                break;

            case 2 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                action[7] = "§2▇";
                action[8] = "§2▇";
                break;

            case 1 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                action[7] = "§2▇";
                action[8] = "§2▇";
                action[9] = "§2▇";
                break;
            case -1 :
                action[0] = "";
                action[1] = "";
                action[2] = "";
                action[3] = "";
                action[4] = "";
                action[5] = "";
                action[6] = "";
                action[7] = "";
                action[8] = "";
                action[9] = "";
                break;
            default:
                action = action;
        }

        String msg = "";

        int bucle1 = 0;
        while (bucle1 < action.length) {
            msg = msg+action[bucle1];
            bucle1++;
        }

        UtilChat.sendActionBar(new ChatText(msg), player);

        //Action.playAction(player, msg);
        //ActionBarAPI.sendActionBar(player, action.toString());
    }


}
