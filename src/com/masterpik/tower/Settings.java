package com.masterpik.tower;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import java.util.ArrayList;

public class Settings {

    public static ArrayList<Material> cantBreak;
    public static ArrayList<Material> cantCraft;
    public static Integer minY;
    public static Location lobbyLocation;
    public static Location middleLocation;

    public static ArrayList<Location> spawners;

    public static int pointToWin;

    public static float volumeSound = (float) 1.0;
    public static float pitchSound = (float) 1.0;

    public static void settingsInit() {

        cantBreakInit();
        canCraftInit();

        minY = 170;

        lobbyLocation = new Location(Bukkit.getWorld("lobby"), 0.5, 177, 0.5, 0, 0);

        spawners = new ArrayList<>();
        spawners.add(new Location(Bukkit.getWorld("world"), -1.5, 209.5, -1.5));
        spawners.add(new Location(Bukkit.getWorld("world"), -1.5, 209.5, 2.5));
        spawners.add(new Location(Bukkit.getWorld("world"), 2.5, 209.5, 2.5));
        spawners.add(new Location(Bukkit.getWorld("world"), 2.5, 209.5, -1.5));

        pointToWin = 10;

    }

    public static void cantBreakInit() {
        cantBreak = new ArrayList<Material>();
        cantBreak.add(Material.STAINED_GLASS);
        cantBreak.add(Material.GLASS);
        //cantBreak.add(Material.FENCE);
        cantBreak.add(Material.DAYLIGHT_DETECTOR);
        cantBreak.add(Material.DAYLIGHT_DETECTOR_INVERTED);
        cantBreak.add(Material.CHEST);
        cantBreak.add(Material.BREWING_STAND);
        //cantBreak.add(Material.QUARTZ_STAIRS);
        //cantBreak.add(Material.IRON_BLOCK);
        cantBreak.add(Material.EMERALD_BLOCK);
        cantBreak.add(Material.BANNER);
        cantBreak.add(Material.WALL_BANNER);
        cantBreak.add(Material.BEACON);
    }

    public static void canCraftInit() {
        cantCraft = new ArrayList<>();
        cantCraft.add(Material.SHEARS);
        cantCraft.add(Material.QUARTZ_STAIRS);
    }


}
