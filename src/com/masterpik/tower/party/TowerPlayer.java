package com.masterpik.tower.party;

import org.bukkit.entity.Player;

public class TowerPlayer {

    Player player;
    Team team;
    Party party;

    boolean freeze;

    int kills;
    int deaths;
    int pointMark;

    boolean respawnProt;


    public TowerPlayer(Player Pplayer, Team Pteam, Party Pparty) {
        this.player = Pplayer;
        this.team = Pteam;
        this.party = Pparty;

        this.freeze = false;

        this.kills = 0;
        this.deaths = 0;
        this.pointMark = 0;

        respawnProt = false;
    }

    public Player getPlayer() {
        return player;
    }
    public void setPlayer(Player Pplayer) {
        this.player = Pplayer;
    }

    public Team getTeam() {
        return team;
    }
    public void setTeam(Team Pteam) {
        this.team = Pteam;
    }

    public Party getParty() {
        return party;
    }
    public void setParty(Party Pparty) {
        this.party = Pparty;
    }

    public boolean isFreeze() {
        return freeze;
    }
    public void setFreeze(boolean Pfreeze) {
        this.freeze = Pfreeze;
    }


    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public int getPointMark() {
        return pointMark;
    }

    public void setPointMark(int pointMark) {
        this.pointMark = pointMark;
    }

    public boolean isRespawnProt() {
        return respawnProt;
    }

    public void setRespawnProt(boolean respawnProt) {
        this.respawnProt = respawnProt;
    }
}
