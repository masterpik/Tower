package com.masterpik.tower.party;

import com.masterpik.api.specialEffects.Fireworks;
import com.masterpik.api.util.UtilPlayer;
import com.masterpik.tower.*;
import com.masterpik.tower.bungee.BungeeWork;
import com.masterpik.tower.chat.ChatManagement;
import com.masterpik.tower.chat.Messages;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.HashMap;

public class PlayerManagement {

    public static HashMap<Player, TowerPlayer> players;

    public static void PlayersInit() {
        players = new HashMap<>();

    }

    public static boolean isRegister(Player player) {
        boolean reg = false;

        if (PlayerManagement.players != null) {
            reg = PlayerManagement.players.containsKey(player);
        }

        return reg;
    }

    public static TowerPlayer playerToTowerPlayer(Player player) {
        if (PlayerManagement.isRegister(player)) {
            return players.get(player);
        }
        else {
            return null;
        }
    }

    public static void playerJoin(Player player) {
        TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer(player);

        if (Tplayer != null) {

            Party party = Tplayer.getParty();
            Team team = Tplayer.getTeam();

            player.setGameMode(GameMode.ADVENTURE);
            UtilPlayer.realPlayerClear(player);

            player.setPlayerListName(TeamManagement.colorToChatString(team.getColor()) + "§l" + player.getName());

            if (party.getPlayers().size() == (party.getNbTeam() * party.getNbPlayer())) {
                PartyManagement.startParty(party);
            }

            player.teleport(Settings.lobbyLocation);


            ChatManagement.sendMessage("@" + Messages.Join + " §r§6§l[" + Integer.toString(party.getPlayers().size()) + "/" + Integer.toString((party.getNbPlayer() * party.getNbTeam())) + "]", player);

            Utils.setArmor(player);

            Utils.showPlayersPlayer(player);

            Utils.setAllPartyXp(party, 0);


            player.setScoreboard(ScoreboardManagement.addTeams(player));

            LobbyItems.giveLobbyItems(player);

            if (Main.spectators != null || Main.spectators.size() != 0) {
                int bcl = 0;
                while (bcl < Main.spectators.size()) {
                    player.hidePlayer(Main.spectators.get(bcl));
                    Main.spectators.get(bcl).showPlayer(player);
                    bcl++;
                }
            }
        }

    }

    public static void playerQuit(Player player) {

        if (PlayerManagement.players.containsKey(player)) {

            TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer(player);
            Party party = Tplayer.getParty();
            Team team = Tplayer.getTeam();

            ChatManagement.sendMessage("@" + Messages.Quit, player);

            PartyStatistics.playerQuit(Tplayer);

            ArrayList<String> retur = new ArrayList<>();
            retur.add(0, "hub");
            retur.add(1, "tower");
            retur.add(2, Integer.toString(Tplayer.getParty().getNbTeam()));
            retur.add(3, Integer.toString(Tplayer.getParty().getNbPlayer()));

            BungeeWork.sendToGameMessage(player, retur);

            party.getPlayers().remove(player);
            team.getPlayers().remove(player);

            if (party.isStart()
                    && !player.getLocation().getWorld().equals(Settings.lobbyLocation.getWorld())) {
                if (player.getInventory().getContents() != null
                        && player.getInventory().getContents().length != 0) {
                    for (ItemStack item : player.getInventory().getContents()) {
                        if (item != null) {
                            player.getLocation().getWorld().dropItem(player.getLocation(), item);
                        }
                    }
                }
            }

            if (party.getPlayers() == null
                    || party.getPlayers().size() == 0
                    || party.getPlayers().isEmpty()
                    || party.isEnd()) {
            } else {
                Team winTeam = PartyManagement.testForWin(Tplayer.getParty());
                if (winTeam != null
                        && winTeam.getPlayers() != null
                        && winTeam.getPlayers().size() > 0
                        && !winTeam.getPlayers().isEmpty()) {
                    PartyManagement.winParty(party, ((TowerPlayer) winTeam.getPlayers().values().toArray()[0]).getPlayer(), winTeam);
                }
            }
            PlayerManagement.players.remove(player);

            if (party.getPlayers().size() == 0
                    || party.getPlayers() == null) {
                Bukkit.getLogger().info("a party delete");
                PartyManagement.deleteParty(party);
            }
        } else {
            if (Main.spectators.contains(player)) {
                Main.spectators.remove(player);
            }
        }
    }

    public static void playerDied(Player player, EntityDamageEvent.DamageCause cause, Location diedLocation, String killer) {

        TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer(player);

        if (Tplayer != null) {

            Team team = Tplayer.getTeam();


            Tplayer.getPlayer().spigot().respawn();

            Tplayer.getPlayer().getInventory().remove(Material.BAKED_POTATO);

            Tplayer.getPlayer().teleport(diedLocation);

            if (!Tplayer.getPlayer().getWorld().equals(Bukkit.getWorld("lobby"))) {

                UtilPlayer.realPlayerClear(player);

                String diedName = "";

                if (cause.equals(EntityDamageEvent.DamageCause.FALL)) {
                    diedName = "Degats de chute";
                } else if (cause.equals(EntityDamageEvent.DamageCause.DROWNING)) {
                    diedName = "Noyade";
                } else if (cause.equals(EntityDamageEvent.DamageCause.MAGIC)) {
                    diedName = "Potions";
                } else if (cause.equals(EntityDamageEvent.DamageCause.POISON)) {
                    diedName = "Potions";
                } else if (cause.equals(EntityDamageEvent.DamageCause.VOID)) {
                    diedName = "Vide";
                } else if (cause.equals(EntityDamageEvent.DamageCause.SUFFOCATION)) {
                    diedName = "Bordures";
                } else if (cause.equals(EntityDamageEvent.DamageCause.PROJECTILE)
                        || cause.equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
				/*Player killer = player.getKiller();
				String name = killer.getName();*/

                    TowerPlayer Tkiller = PlayerManagement.playerToTowerPlayer(Bukkit.getPlayer(killer));

                    if (Tkiller != null) {

                        String asassins = "" + TeamManagement.colorToChatString(Tkiller.getTeam().getColor()) + "§o§l§n" + killer;

                        diedName = "Tué par "+asassins+"";

                        Tkiller.setKills(Tkiller.getKills()+1);

                    }


                }

                ChatManagement.sendMessage("@" + Messages.youDied + " §o§n" + diedName, player);

                Tplayer.setDeaths(Tplayer.getDeaths()+1);

                Fireworks.explodeFirework(player.getLocation(), 1, TeamManagement.colorToColor(Tplayer.getTeam().getColor()), false, true);

                BukkitTask time = Bukkit.getServer().getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {

                        player.teleport(team.getRespawnLocation());
                        player.setFoodLevel(20);
                        player.setHealth(20);

                        Utils.setArmor(player);

                        Tplayer.setFreeze(true);
                        Tplayer.setRespawnProt(true);
                        player.setGameMode(GameMode.SPECTATOR);

                        BukkitTask time = Bukkit.getServer().getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                Tplayer.setFreeze(false);
                                Location tp = team.getRespawnLocation().clone();
                                tp.setYaw(player.getLocation().getYaw());
                                tp.setPitch(player.getLocation().getPitch());
                                player.teleport(tp);
                                player.setFoodLevel(20);
                                player.setHealth(20);
                                player.setGameMode(GameMode.SURVIVAL);
                                player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100, 99, true, false));

                                BukkitTask time = Bukkit.getServer().getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() {
                                        Tplayer.setRespawnProt(false);
                                    }
                                }, 20*5);

                            }
                        }, 20*5);


                    }
                }, 1);

            } else {

                Location tp = Settings.lobbyLocation.clone();
                tp.setYaw(Tplayer.getPlayer().getLocation().getYaw());
                tp.setPitch(Tplayer.getPlayer().getLocation().getPitch());

                Tplayer.getPlayer().teleport(tp);

            }
        }

    }


}
