package com.masterpik.tower.party;

import com.masterpik.api.specialEffects.Fireworks;
import com.masterpik.api.util.*;
import com.masterpik.connect.enums.Statistics;
import com.masterpik.connect.enums.Times;
import com.masterpik.tower.Main;
import com.masterpik.tower.ScoreboardManagement;
import com.masterpik.tower.Settings;
import com.masterpik.tower.Utils;
import com.masterpik.tower.bungee.BungeeWork;
import com.masterpik.tower.chat.ChatManagement;
import com.masterpik.tower.chat.Messages;
import org.bukkit.*;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitTask;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class PartyManagement {

    public static ArrayList<Byte> nbTeams;
    public static ArrayList<Byte> nbPlayers;

    public static HashMap<Byte, HashMap<Byte, HashMap<Integer, Party>>> partys;
    public static ArrayList<Party> partyList;



    public static void PartyInit() {

        partyList = new ArrayList<>();
        nbTeams = new ArrayList<>();
        nbPlayers = new ArrayList<>();
        partys = new HashMap<Byte, HashMap<Byte, HashMap<Integer, Party>>>();

        nbTeams.add((byte)2);
        nbTeams.add((byte)4);

        nbPlayers.add((byte)4);
        nbPlayers.add((byte)8);

        partys = new HashMap<Byte,  HashMap<Byte, HashMap<Integer, Party>>>();
        int bucle1 = 0;
        int bucle2 = 0;
        while (bucle1 < nbTeams.size()) {

            partys.put(nbTeams.get(bucle1), new HashMap<>());

            bucle2 = 0;

            while (bucle2 < nbPlayers.size()) {

                partys.get(nbTeams.get(bucle1)).put(nbPlayers.get(bucle2), new HashMap<>());

                bucle2++;
            }

            bucle1++;
        }

    }

    public static void createParty(Byte nbTeam, Byte nbPlayer, int nb) {

        String partyName = Utils.BytesToPartyName(nbTeam, nbPlayer, nb);
        String partySource = Utils.BytesToPartySource(nbTeam, nbPlayer);

        UtilWorld.cloneWorld(partySource, partyName);

        partys.get(nbTeam).get(nbPlayer).put(nb, new Party(partyName, nbTeam, nbPlayer, nb));

        Party party = partys.get(nbTeam).get(nbPlayer).get(nb);

        partyList.add(party);

        PartyManagement.initGamerule(party.getWorld());

        PartyManagement.addWorldBorder(party);

        for (Team team : party.getTeams().values()) {
            for (Banner banner : team.getBanners()) {
                UtilBanners.setNumberOnBanner(banner, 0);
            }
        }

    }

    public static void startParty(Party party) {

        party.setStart(true);
        ArrayList<TowerPlayer> namePlayers = new ArrayList<>();
        namePlayers.addAll(party.getPlayers().values());

        ArrayList<Integer> in = new ArrayList<>();

        in.add((Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
            int bucle = 0;
            int bucle2 = 0;
            int count = 10;
            TowerPlayer player;

            @Override
            public void run() {

                bucle = 0;
                if (party.isForced()
                        || party.getPlayers().size() == (party.getNbTeam() * party.getNbPlayer())) {
                    while (bucle < namePlayers.size()) {

                        player = namePlayers.get(bucle);

                        if (count > 1) {
                            if (bucle == 0) {
                                if (count == 10) {
                                    ChatManagement.brodcasteMessage("" + Messages.countRebour + "" + count + " §r§a§lsecondes", player.getPlayer());
                                }
                            }
                            //Utils.setAllPartyXp(player.getPlayer(), count);
                            Utils.sendSecondTitle(player.getPlayer(), Integer.toString(count));
                            Utils.actionBarSecond(player.getPlayer(), count);
                            player.getPlayer().playSound(player.getPlayer().getLocation(), Sound.BLOCK_NOTE_PLING, Settings.volumeSound, Settings.pitchSound);
                        } else if (count == 1) {
                            if (bucle == 0) {
                                if (count == 10) {
                                    ChatManagement.brodcasteMessage("" + Messages.countRebour + "" + count + " §r§a§lseconde", player.getPlayer());
                                }
                            }
                            //Utils.setAllPartyXp(player.getPlayer(), count);
                            Utils.sendSecondTitle(player.getPlayer(), Integer.toString(count));
                            Utils.actionBarSecond(player.getPlayer(), count);
                            player.getPlayer().playSound(player.getPlayer().getLocation(), Sound.BLOCK_NOTE_PLING, Settings.volumeSound, Settings.pitchSound);
                        } else {
                            if (bucle == 0) {
                                ChatManagement.brodcasteMessage(Messages.partyStart, player.getPlayer());
                            }

                            player.getPlayer().playSound(player.getPlayer().getLocation(), Sound.BLOCK_NOTE_PLING, Settings.volumeSound, Settings.pitchSound+1);

                            player.getPlayer().setBedSpawnLocation(player.getTeam().getRespawnLocation(), true);
                            player.getPlayer().teleport(player.getTeam().getRespawnLocation());
                            player.getPlayer().setGameMode(GameMode.SURVIVAL);


                            UtilPlayer.realPlayerClear(player.getPlayer());

                            Utils.setArmor(player.getPlayer());


                            player.getPlayer().sendMessage(Messages.startingMessageDescription);
                        }

                        bucle++;
                    }

                    if (count > 0) {
                        count--;
                    } else {
                        party.setStartTime(UtilDate.getCurrentDate().getTime());
                        Bukkit.getScheduler().cancelTask(in.get(0));
                        Utils.setAllPartyXp(party, 0);
                        Utils.sendSecondTitle(party, Messages.partyStartTitle);
                        Utils.actionBarSecond(party, -1);
                    }
                } else {
                    Utils.setAllPartyXp(party, 0);
                    party.setStart(false);

                    ArrayList<TowerPlayer> pllist = new ArrayList<>();
                    pllist.addAll(party.getPlayers().values());
                    int bcl1 = 0;
                    while (bcl1 < pllist.size()) {
                        pllist.get(bcl1).getPlayer().setScoreboard(Main.scoreboard.getNewScoreboard());
                        bcl1++;
                    }

                    Bukkit.getScheduler().cancelTask(in.get(0));
                }

            }
        }, 0, 20)));

    }

    public static void winParty(Party party, Player player, Team winTeam) {
        ArrayList<TowerPlayer> players = new ArrayList<>();
        players.addAll(party.getPlayers().values());

        if (!player.getWorld().equals(Bukkit.getWorld("lobby"))
                || !players.get(0).getPlayer().getWorld().equals(Bukkit.getWorld("lobby"))) {
            ChatManagement.brodcasteMessage("" + Messages.win1 + "" + TeamManagement.colorToChatString(winTeam.getColor()) + "§l" + TeamManagement.colorToString(winTeam.getColor()) + "" + Messages.win2 + "", player);

            party.setWin(true);
            party.setWinTeam(winTeam);
            party.setEndTime(UtilDate.getCurrentDate().getTime());

            for (TowerPlayer pl : players) {
                pl.getPlayer().setGameMode(GameMode.SPECTATOR);

                pl.getPlayer().sendMessage("\n"+Messages.main+(Statistics.PIK.getMessage().replaceAll("%v", Integer.toString(PartyStatistics.getPiksWin(pl, (pl.getTeam() == party.getWinTeam()))))));
                pl.getPlayer().sendMessage(Messages.main+(Statistics.SPENT_TIME.getMessage().replaceAll("%v", Times.getTime(UtilDate.millisecondsToMinutes(party.getEndTime()-party.getStartTime())))));
                pl.getPlayer().sendMessage(Messages.main+(Statistics.KILL_AMOUNT.getMessage().replaceAll("%v", Integer.toString(pl.getKills()))));
                pl.getPlayer().sendMessage(Messages.main+(Statistics.DEATH_AMOUNT.getMessage().replaceAll("%v", Integer.toString(pl.getDeaths()))));
                pl.getPlayer().sendMessage(Messages.main+(Statistics.POINT_MARK_AMOUNT.getMessage().replaceAll("%v", Integer.toString(pl.getPointMark()))));
                pl.getPlayer().sendMessage(Messages.main+Messages.statsRegister+"\n ");
            }

            BukkitTask time = Bukkit.getServer().getScheduler().runTaskLater(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    int bucle = 0;

                    if (!party.isEnd()) {

                        while (bucle < players.size()) {

                            players.get(bucle).getPlayer().sendMessage(Messages.endParty);
                            BungeeWork.sendPlayer(players.get(bucle).getPlayer(), "hub");

                            bucle++;
                        }

                    }
                }
            }, 100);
        }

    }

    public static void deleteParty(Party party) {

        int bucl = 0;
        while (bucl < Main.spectators.size()) {
            //Bukkit.getPlayer("axroy").sendRawMessage(Main.spectators.get(bucl).getWorld().getName()+" | "+next);
            if (Main.spectators.get(bucl).getWorld().getName().equalsIgnoreCase(party.getWorld().getName())) {
                Main.spectators.get(bucl).teleport(Settings.lobbyLocation);
            }
            bucl ++;
        }

        UtilWorld.deleteWorld(party.getName());

        Bukkit.getScheduler().runTaskLater(com.masterpik.api.Main.plugin, new Runnable() {
            @Override
            public void run() {
                partys.get(party.getNbTeam()).get(party.getNbPlayer()).remove(party.getNb());
                partyList.remove(party);
            }
        }, 20L);

    }

    public static int getNext(Byte nbTeam, Byte nbPlayer) {
        int bucle1 = 0;
        boolean scdBcl = false;
        ArrayList<Party> parti = new ArrayList<>();
        parti.addAll(partys.get(nbTeam).get(nbPlayer).values());

        if (parti.isEmpty()) {
            return 1;
        }
        else {

            while (bucle1 < parti.size()){

                if (!scdBcl) {
                    if (!parti.get(bucle1).isStart()) {
                        return parti.get(bucle1).getNb();
                    } else if (bucle1 == (parti.size() - 1)) {
                        bucle1 --;
                        scdBcl = true;
                    }
                }
                else {
                    if (!partys.get(nbTeam).get(nbPlayer).containsKey(parti.get(bucle1).getNb()+1)){
                        return parti.get(bucle1).getNb()+1;
                    }
                }

                bucle1++;
            }

        }
        return 1;
    }

    public static void playerMark(Player player) {
        Block walkedBlock = player.getLocation().getBlock().getRelative(BlockFace.DOWN);
        Block walkedBlockDown = walkedBlock.getRelative(BlockFace.DOWN);

        TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer(player);
        if (Tplayer != null) {
            Team team = Tplayer.getTeam();
            Party party = Tplayer.getParty();

            if (Tplayer.getTeam().getColor() != walkedBlockDown.getData()) {

                team.setPoint(team.getPoint() + 1);

                Fireworks.explodeFirework(player.getLocation(), 2, TeamManagement.colorToColor(Tplayer.getTeam().getColor()), false, true);

                ScoreboardManagement.refreshScoreboard(party);

                ChatManagement.brodcasteMessage("" + Messages.playerMark1 + TeamManagement.colorToChatString(team.getColor()) + "§l" + TeamManagement.colorToString(team.getColor()) + Messages.playerMark2 + TeamManagement.colorToChatString(team.getColor()) + "§l§o(" + player.getName() + ")", player);

                Tplayer.setPointMark(Tplayer.getPointMark()+1);

                Tplayer.getPlayer().teleport(team.getRespawnLocation());

                for (Banner banner : team.getBanners()) {
                    UtilBanners.setNumberOnBanner(banner, team.getPoint());
                }

                for (Player pl : Tplayer.getParty().getTeams().get(walkedBlockDown.getData()).getPlayers().keySet()) {
                    if (pl.getHealth() >= 2) {
                        pl.setHealth(pl.getHealth() - (pl.getHealth() / 4));
                    }
                }


                int bucle = 0;

                ArrayList<Player> teamPlayers = new ArrayList<>();
                teamPlayers.addAll(team.getPlayers().keySet());

                while (bucle < teamPlayers.size()) {

                    teamPlayers.get(bucle).playSound(teamPlayers.get(bucle).getLocation(), Sound.ENTITY_PLAYER_LEVELUP, Settings.volumeSound + 1, Settings.pitchSound);

                    /*for (ItemStack item : teamPlayers.get(bucle).getInventory().getContents()) {
                        if (item != null
                                && !item.getType().equals(Material.AIR)
                                && item.getType().equals(Material.SHIELD)) {
                            item.setItemMeta(UtilShield.patternOnShield(new ItemStack(Material.SHIELD), DyeColor.getByColor(TeamManagement.colorToColor(team.getColor())), null).getItemMeta());
                        }
                    }*/


                    bucle++;
                }



                Tplayer.getPlayer().setHealth(20);
                Tplayer.getPlayer().setFoodLevel(20);

                if (party.getPlayers().size() == 0) {
                } else {
                    Team winTeam = PartyManagement.testForWin(Tplayer.getParty());
                    if (winTeam != null) {
                        PartyManagement.winParty(party, player, winTeam);
                    }
                }

            } else {
                Tplayer.getPlayer().teleport(team.getRespawnLocation());
            }
        }

    }

    public static void initGamerule(World world) {

        String trou = "true";
        String folse = "false";

        world.setFullTime(6000L);

        world.setGameRuleValue("commandBlockOutput", folse);
        world.setGameRuleValue("doDaylightCycle", folse);
        world.setGameRuleValue("doEntityDrops", folse);
        world.setGameRuleValue("doFireTick", folse);
        world.setGameRuleValue("doMobLoot", folse);
        world.setGameRuleValue("doMobSpawning", folse);
        world.setGameRuleValue("doTileDrops", trou);
        world.setGameRuleValue("keepInventory", folse);
        world.setGameRuleValue("logAdminCommands", trou);
        world.setGameRuleValue("mobGriefing", folse);
        world.setGameRuleValue("naturalRegeneration", trou);
        world.setGameRuleValue("randomTickSpeed", "3");
        world.setGameRuleValue("reducedDebugInfo", trou);
        world.setGameRuleValue("sendCommandFeedback", folse);
        world.setGameRuleValue("showDeathMessages", folse);

        world.setAmbientSpawnLimit(0);
        world.setAnimalSpawnLimit(0);
        world.setMonsterSpawnLimit(0);
        world.setWaterAnimalSpawnLimit(0);

        world.setAutoSave(false);

        if (world.getName() == "lobby") {
            world.setPVP(false);
            world.setDifficulty(Difficulty.PEACEFUL);
        } else {
            world.setDifficulty(Difficulty.NORMAL);
            world.setPVP(true);
        }
        /*Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                "mvmset animals false "+world.getName());
        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                "mvmset monsters false "+world.getName());

        if (world.getName() == "lobby") {
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                    "mvmset pvp false "+world.getName());
        } else {
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                    "mvmset pvp true "+world.getName());
        }

        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                "mvmset diff 2 "+world.getName());
        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                "mvmset weather false "+world.getName());*/
    }

    public static void addWorldBorder(Party party) {

        WorldBorder worldBorder = party.getWorld().getWorldBorder();

        worldBorder.setCenter(0, 0);
        worldBorder.setSize(150);
        worldBorder.setWarningDistance(5);
        worldBorder.setWarningTime(10);
        worldBorder.setDamageAmount(3);
        worldBorder.setDamageBuffer(1);

    }

    public static void changeTeamPlayer(TowerPlayer player, ItemStack banner) {

        Team newcolor = player.getParty().getTeams().get(TeamManagement.bannerByteToByte(banner.getData().getData()));
        Team oldColor = player.getTeam();

        if (newcolor.getColor() != oldColor.getColor()) {
            player.getPlayer().closeInventory();
            if (newcolor.getPlayers().size() < (int) player.getParty().getNbPlayer()) {

                player.setTeam(newcolor);

                oldColor.getPlayers().remove(player.getPlayer());
                newcolor.getPlayers().put(player.getPlayer(), player);

                player.getPlayer().setPlayerListName(TeamManagement.colorToChatString(player.getTeam().getColor()) + "§l" + player.getPlayer().getName());

                ScoreboardManagement.refreshScoreboard(player.getParty());

                Utils.setArmor(player.getPlayer());

                player.getPlayer().sendMessage(Messages.joinTeam+TeamManagement.colorToChatString(newcolor.getColor())+"§l§o"+TeamManagement.colorToString(newcolor.getColor()));
            }
            else {
                player.getPlayer().sendMessage(Messages.teamIsFull1+TeamManagement.colorToChatString(newcolor.getColor())+"§l§o"+TeamManagement.colorToString(newcolor.getColor())+Messages.teamIsFull2);
            }
        }

    }

    public static Team testForWin(Party party) {

        Team winner = null;

        int bucle1 = 0;
        int teamWithoutPlayers = 0;

        ArrayList<Team> teams = new ArrayList<>();
        teams.addAll(party.getTeams().values());

        while (bucle1 < party.getNbTeam()) {

            if (teams.get(bucle1).getPlayers() == null
                    || teams.get(bucle1).getPlayers().size() == 0) {
                teamWithoutPlayers ++;
            }

            if (teams.get(bucle1).getPoint() >= Settings.pointToWin) {

                winner = teams.get(bucle1);

                bucle1 = 10000;
            }

            bucle1++;
        }

        if (winner == null
                && teamWithoutPlayers == (party.getNbTeam() - 1)) {
            ArrayList<TowerPlayer> players = new ArrayList<>();
            players.addAll(party.getPlayers().values());

            winner = players.get(0).getTeam();

        }

        return winner;
    }
}
