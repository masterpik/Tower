package com.masterpik.tower.party;

import org.bukkit.Location;
import org.bukkit.block.Banner;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class Team {


    byte color;

    HashMap<Player, TowerPlayer> players;

    Location respawnLocation;

    int point;

    ArrayList<Banner> banners;

    public Team(byte Pcolor, Location PrespawnLocation) {

        this.color = Pcolor;

        this.players = new HashMap<Player, TowerPlayer>();

        this.respawnLocation = PrespawnLocation;

        this.point = 0;

        this.banners = new ArrayList<>();

    }


    public byte getColor() {
        return color;
    }
    public void setColor(byte Pcolor) {
        this.color = Pcolor;
    }


    public HashMap<Player, TowerPlayer> getPlayers() {
        return players;
    }
    public void setPlayers(HashMap<Player, TowerPlayer> Pplayers) {
        this.players = Pplayers;
    }

    public Location getRespawnLocation() {
        return respawnLocation;
    }
    public void setRespawnLocation(Location PrespawnLocation) {
        this.respawnLocation = PrespawnLocation.clone();
    }

    public int getPoint() {
        return point;
    }
    public void setPoint(int Ppoint) {
        this.point = Ppoint;
    }

    public ArrayList<Banner> getBanners() {
        return banners;
    }

    public Team setBanners(ArrayList<Banner> banners) {
        this.banners = banners;
        return this;
    }
}
