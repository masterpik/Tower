package com.masterpik.tower.party;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Banner;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class Party {

    String name;

    World world;

    byte nbTeam;
    byte nbPlayer;
    int nb;

    HashMap<Player, TowerPlayer> players;
    HashMap<Byte, Team> teams;

    boolean start;
    boolean end;
    boolean forced;
    boolean win;

    Team winTeam;

    long startTime;
    long endTime;

    public Party(String Pname, Byte PnbTeam, Byte PnbPlayer, int Pnb) {

        this.name = Pname;

        this.world = Bukkit.getWorld(this.name);

        this.nbTeam = PnbTeam;
        this.nbPlayer = PnbPlayer;
        this.nb = Pnb;

        this.players = new HashMap<Player, TowerPlayer>();
        this.teams = new HashMap<Byte, Team>();


        if (PnbTeam == (byte) 2) {
            teams.put((byte) 14, new Team((byte)14, new Location(this.world, -66.5, 198.5, 0.5, -90, 0)));
            this.teams.get((byte)14).getBanners().add((Banner) this.world.getBlockAt(56, 201, -1).getState());
            this.teams.get((byte)14).getBanners().add((Banner) this.world.getBlockAt(-56, 201, -1).getState());

            teams.put((byte) 11, new Team((byte)11, new Location(this.world, 67.5, 197.5, 0.5, 90, 0)));
            this.teams.get((byte)11).getBanners().add((Banner) this.world.getBlockAt(56, 201, 1).getState());
            this.teams.get((byte)11).getBanners().add((Banner) this.world.getBlockAt(-56, 201, 1).getState());
        } else if (PnbTeam == (byte) 4) {
            teams.put((byte) 14, new Team((byte)14, new Location(this.world, -66.5, 198.5, 0.5, -90, 0)));
            this.teams.get((byte)14).getBanners().add((Banner) this.world.getBlockAt(-56, 201, -1).getState());
            this.teams.get((byte)14).getBanners().add((Banner) this.world.getBlockAt(-3, 201, -56).getState());
            this.teams.get((byte)14).getBanners().add((Banner) this.world.getBlockAt(56, 201, -1).getState());
            this.teams.get((byte)14).getBanners().add((Banner) this.world.getBlockAt(-3, 201, 56).getState());

            teams.put((byte) 5, new Team((byte)5, new Location(this.world, 0.5, 198.5, -66.5, 0, 0)));
            this.teams.get((byte)5).getBanners().add((Banner) this.world.getBlockAt(-56, 201, -3).getState());
            this.teams.get((byte)5).getBanners().add((Banner) this.world.getBlockAt(1, 201, -56).getState());
            this.teams.get((byte)5).getBanners().add((Banner) this.world.getBlockAt(56, 201, -3).getState());
            this.teams.get((byte)5).getBanners().add((Banner) this.world.getBlockAt(1, 201, 56).getState());

            teams.put((byte) 4, new Team((byte)4, new Location(this.world, 67.5, 198, 0.5, 90, 0)));
            this.teams.get((byte)4).getBanners().add((Banner) this.world.getBlockAt(-56, 201, 1).getState());
            this.teams.get((byte)4).getBanners().add((Banner) this.world.getBlockAt(3, 201, -56).getState());
            this.teams.get((byte)4).getBanners().add((Banner) this.world.getBlockAt(56, 201, 1).getState());
            this.teams.get((byte)4).getBanners().add((Banner) this.world.getBlockAt(3, 201, 56).getState());

            teams.put((byte) 11, new Team((byte)11, new Location(this.world, 0.5, 198.5, 67.5, 180, 0)));
            this.teams.get((byte)11).getBanners().add((Banner) this.world.getBlockAt(-56, 201, 3).getState());
            this.teams.get((byte)11).getBanners().add((Banner) this.world.getBlockAt(-1, 201, -56).getState());
            this.teams.get((byte)11).getBanners().add((Banner) this.world.getBlockAt(56, 201, 3).getState());
            this.teams.get((byte)11).getBanners().add((Banner) this.world.getBlockAt(-1, 201, 56).getState());
        }

        this.start = false;
        this.end = false;
        this.forced = false;
        this.win = false;

        this.winTeam = null;

        this.startTime = 0;
        this.endTime = 0;

    }


    public String getName() {
        return name;
    }
    public void setName(String Pname) {
        this.name = Pname;
    }

    public World getWorld() {
        return world;
    }
    public void setWorld(World Pworld) {
        this.world = Pworld;
    }


    public byte getNbTeam() {
        return nbTeam;
    }
    public void setNbTeam(byte PnbTeam) {
        this.nbTeam = PnbTeam;
    }

    public byte getNbPlayer() {
        return nbPlayer;
    }
    public void setNbPlayer(byte PnbPlayer) {
        this.nbPlayer = PnbPlayer;
    }

    public int getNb() {
        return nb;
    }
    public void setNb(int Pnb) {
        this.nb = Pnb;
    }

    public HashMap<Player, TowerPlayer> getPlayers() {
        return players;
    }
    public void setPlayers(HashMap<Player, TowerPlayer> Pplayers) {
        this.players = Pplayers;
    }

    public HashMap<Byte, Team> getTeams() {
        return teams;
    }
    public void setTeams(HashMap<Byte, Team> Pteams) {
        this.teams = Pteams;
    }


    public boolean isStart() {
        return start;
    }
    public void setStart(boolean Pstart) {
        this.start = Pstart;
    }

    public boolean isEnd() {
        return end;
    }
    public void setEnd(boolean Pend) {
        this.end = Pend;
    }

    public boolean isForced() {
        return forced;
    }
    public void setForced(boolean Pforced) {
        this.forced = Pforced;
    }

    public boolean isWin() {
        return win;
    }
    public void setWin(boolean win) {
        this.win = win;
    }

    public Team getWinTeam() {
        return winTeam;
    }
    public void setWinTeam(Team winTeam) {
        this.winTeam = winTeam;
    }

    public long getStartTime() {
        return startTime;
    }
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }
    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
}
