package com.masterpik.tower.party;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;

public class TeamManagement {

    public static Team randomizeTeam(Party party) {
        ArrayList<Team> teams = new ArrayList<>();
        teams.addAll(party.getTeams().values());

        int nbMaxPlayer = party.getNbPlayer()/party.getNbTeam();

        int bucle1 = 0;

        while (bucle1 < teams.size()) {

            if (teams.size() == nbMaxPlayer) {
                teams.remove(teams.get(bucle1));
                bucle1--;
            }

            bucle1++;
        }

        Collections.shuffle(teams);

        return teams.get(0);
    }

    public static String colorToString(Byte color) {

        String colorS = "";

        switch (color) {

            case 0 :
                colorS = "Blanc";
                break;
            case 15 :
                colorS = "Noir";
                break;
            case 14 :
                colorS = "Rouge";
                break;
            case 5 :
                colorS = "Vert";
                break;
            case 4 :
                colorS = "Jaune";
                break;
            case 11 :
                colorS = "Bleu";
                break;
            default:
                colorS = "ERROR";

        }

        //colorS.toUpperCase();

        return colorS;
    }

    public static ChatColor colorToChatColor(Byte color) {

        ChatColor colorS;

        switch (color) {

            case 0 :
                colorS = ChatColor.WHITE;
                break;
            case 15 :
                colorS = ChatColor.DARK_GRAY;
                break;
            case 14 :
                colorS = ChatColor.RED;
                break;
            case 5 :
                colorS = ChatColor.GREEN;
                break;
            case 4 :
                colorS = ChatColor.YELLOW;
                break;
            default:
                colorS = ChatColor.DARK_RED;

        }

        return colorS;
    }

    public static Color colorToColor(Byte color) {

        Color colorS;

        switch (color) {

            case 0 :
                colorS = Color.WHITE;
                break;
            case 15 :
                colorS = Color.BLACK;
                break;
            case 14 :
                colorS = Color.RED;
                break;
            case 5 :
                colorS = Color.GREEN;
                break;
            case 4 :
                colorS = Color.YELLOW;
                break;
            default:
                colorS = Color.AQUA;

        }

        return colorS;
    }

    public static String colorToChatString(Byte color) {

        String colorS = "";

        switch (color) {

            case 0 :
                colorS = "§f";
                break;
            case 15 :
                colorS = "§8";
                break;
            case 14 :
                colorS = "§c";
                break;
            case 5 :
                colorS = "§a";
                break;
            case 4 :
                colorS = "§e";
                break;
            case 11 :
                colorS = "§b";
                break;

        }

        return colorS;
    }

    public static Byte byteToBannerByte(Byte color) {
        Byte colorS = (byte) 0;

        switch (color) {

            case 0 :
                colorS = (byte) 15;
                break;
            case 15 :
                colorS = (byte) 0;
                break;
            case 14 :
                colorS = (byte) 1;
                break;
            case 5 :
                colorS = (byte) 10;
                break;
            case 4 :
                colorS = (byte) 11;
                break;
            case 11 :
                colorS = (byte) 12;
                break;
            default:
                colorS = (byte) 0;

        }

        return colorS;
    }

    public static Byte bannerByteToByte(Byte color) {
        Byte colorS = (byte) 0;

        switch (color) {

            case 15 :
                colorS = (byte) 0;
                break;
            case 0 :
                colorS = (byte) 15;
                break;
            case 1 :
                colorS = (byte) 14;
                break;
            case 10 :
                colorS = (byte) 5;
                break;
            case 11 :
                colorS = (byte) 4;
                break;
            case 12 :
                colorS = (byte) 11;
                break;
            default:
                colorS = (byte) 0;

        }

        return colorS;
    }

    public static String colorToFix(Byte color) {
        String colorS = "";

        switch (color) {

            case 0 :
                colorS = " ";
                break;
            case 15 :
                colorS = " ";
                break;
            case 14 :
                colorS = "";
                break;
            case 5 :
                colorS = " ";
                break;
            case 4 :
                colorS = "";
                break;
            case 11 :
                colorS = " ";
                break;
            default:
                colorS = "ERROR";

        }

        //colorS.toUpperCase();

        return colorS;
    }

}
