package com.masterpik.tower.party;

import com.masterpik.api.util.UtilDate;
import com.masterpik.api.util.UtilInteger;
import com.masterpik.connect.api.PlayersStatistics;
import com.masterpik.connect.enums.Statistics;
import com.masterpik.connect.enums.StatsPiks;
import com.masterpik.connect.enums.StatsPlayers;

import java.util.ArrayList;
import java.util.UUID;

public class PartyStatistics {

    public static ArrayList<Integer> getPiks(TowerPlayer player) {
        Party party = player.getParty();
        ArrayList<Integer> piks = new ArrayList<>();
        piks.add(StatsPiks.PARTY.getPiks());
        piks.add(StatsPiks.TOWER.getPiks());
        piks.add((int) party.getNbTeam());
        piks.add(11-party.getNbPlayer());
        return piks;
    }

    public static int getPiksWin(TowerPlayer player, boolean winStatu) {
        return PlayersStatistics.getPikPointsWin(UtilInteger.addInts(getPiks(player)), winStatu);
    }

    public static int getPiksWin(TowerPlayer player) {
        return UtilInteger.addInts(getPiks(player));
    }

    public static void playerQuit(TowerPlayer player) {
        Party party = player.getParty();

        UUID uuid = player.getPlayer().getUniqueId();

        if (party.isStart()) {

            long diff;

            if (party.isWin()) {

                diff = party.getEndTime()-party.getStartTime();



                if (player.getTeam() == party.getWinTeam()) {
                    PlayersStatistics.incrementStatistic(StatsPlayers.TOWER, uuid, Statistics.WIN_AMOUNT);
                    PlayersStatistics.addPikPoint(uuid, getPiksWin(player), StatsPlayers.TOWER, true);
                } else {
                    PlayersStatistics.incrementStatistic(StatsPlayers.TOWER, uuid, Statistics.LOST_AMOUNT);
                    PlayersStatistics.addPikPoint(uuid, getPiksWin(player), StatsPlayers.TOWER, false);
                }



            } else {

                diff = UtilDate.getCurrentDate().getTime()-party.getStartTime();

            }
            PlayersStatistics.addSpentTime(uuid, diff, StatsPlayers.TOWER);


            PlayersStatistics.incrementStatistic(StatsPlayers.TOWER, uuid, Statistics.KILL_AMOUNT, player.getKills());
            PlayersStatistics.incrementStatistic(StatsPlayers.TOWER, uuid, Statistics.DEATH_AMOUNT, player.getDeaths());
            PlayersStatistics.incrementStatistic(StatsPlayers.TOWER, uuid, Statistics.POINT_MARK_AMOUNT, player.getPointMark());


            PlayersStatistics.incrementStatistic(StatsPlayers.TOWER, uuid, Statistics.GAMES_AMOUNT);
        }
    }

}
