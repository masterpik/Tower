package com.masterpik.tower.spawners;

import com.masterpik.tower.Settings;
import com.masterpik.tower.party.Party;
import com.masterpik.tower.party.PartyManagement;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftTippedArrow;
import org.bukkit.entity.Item;
import org.bukkit.entity.TippedArrow;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ItemSpawnerRunnable extends BukkitRunnable{

    @Override
    public void run() {

        if (PartyManagement.partyList.size() != 0
                || PartyManagement.partyList != null) {

            int bucle = 0;

            while (bucle < PartyManagement.partyList.size()) {

                int bucle1 = 0;

                while (bucle1 < 4) {

                    Item item = PartyManagement.partyList.get(bucle).getWorld().dropItem(
                            new Location(
                            PartyManagement.partyList.get(bucle).getWorld(),
                            Settings.spawners.get(bucle1).getX(),
                            Settings.spawners.get(bucle1).getY(),
                            Settings.spawners.get(bucle1).getZ()),

                            new ItemStack(Material.IRON_INGOT));

                    item.setVelocity(item.getVelocity().zero());

                    ArrayList<ItemStack> arrows = new ArrayList<>();
                    arrows.addAll(ItemSpawnerManagement.arrows);

                    Collections.shuffle(arrows);

                    Item item2 = PartyManagement.partyList.get(bucle).getWorld().dropItem(
                            new Location(
                                    PartyManagement.partyList.get(bucle).getWorld(),
                                    Settings.spawners.get(bucle1).getX(),
                                    Settings.spawners.get(bucle1).getY()-6,
                                    Settings.spawners.get(bucle1).getZ()),

                            arrows.get(0));

                    item2.setVelocity(item2.getVelocity().zero());

                    bucle1++;
                }

                bucle++;
            }
        }

    }

}
