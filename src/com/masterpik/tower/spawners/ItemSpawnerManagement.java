package com.masterpik.tower.spawners;

import com.masterpik.tower.Main;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ItemSpawnerManagement {


    public static ArrayList<ItemStack> arrows;
    private static BukkitRunnable runnable;

    public static void ItemSpawnerIinit() {
        initarrow();
        runnable = new ItemSpawnerRunnable();
    }

    public static void startTimer() {
        runnable.runTaskTimer(Main.plugin, 0, 300);
    }

    public static void stopTimer() {
        runnable.cancel();
    }

    public static void initarrow() {
        arrows = new ArrayList<>();
        arrows.add(new ItemStack(Material.ARROW));
        arrows.add(new ItemStack(Material.SPECTRAL_ARROW));

        ItemStack tipArrow = new ItemStack(Material.TIPPED_ARROW);
        PotionMeta m = (PotionMeta) tipArrow.getItemMeta();

        m.setBasePotionData(new PotionData(PotionType.INSTANT_DAMAGE));
        tipArrow.setItemMeta(m);
        arrows.add(tipArrow.clone());

        m.setBasePotionData(new PotionData(PotionType.FIRE_RESISTANCE));
        tipArrow.setItemMeta(m);
        arrows.add(tipArrow.clone());

        m.setBasePotionData(new PotionData(PotionType.POISON));
        tipArrow.setItemMeta(m);
        arrows.add(tipArrow.clone());

        m.setBasePotionData(new PotionData(PotionType.INSTANT_HEAL));
        tipArrow.setItemMeta(m);
        arrows.add(tipArrow.clone());

        m.setBasePotionData(new PotionData(PotionType.INVISIBILITY));
        tipArrow.setItemMeta(m);
        arrows.add(tipArrow.clone());

        m.setBasePotionData(new PotionData(PotionType.JUMP));
        tipArrow.setItemMeta(m);
        arrows.add(tipArrow.clone());

        m.setBasePotionData(new PotionData(PotionType.NIGHT_VISION));
        tipArrow.setItemMeta(m);
        arrows.add(tipArrow.clone());

        m.setBasePotionData(new PotionData(PotionType.REGEN));
        tipArrow.setItemMeta(m);
        arrows.add(tipArrow.clone());

        m.setBasePotionData(new PotionData(PotionType.SLOWNESS));
        tipArrow.setItemMeta(m);
        arrows.add(tipArrow.clone());

        m.setBasePotionData(new PotionData(PotionType.SPEED));
        tipArrow.setItemMeta(m);
        arrows.add(tipArrow.clone());

        m.setBasePotionData(new PotionData(PotionType.STRENGTH));
        tipArrow.setItemMeta(m);
        arrows.add(tipArrow.clone());

        m.setBasePotionData(new PotionData(PotionType.WEAKNESS));
        tipArrow.setItemMeta(m);
        arrows.add(tipArrow.clone());
    }

}
