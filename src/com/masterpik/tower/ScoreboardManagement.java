package com.masterpik.tower;

import com.masterpik.tower.chat.Messages;
import com.masterpik.tower.party.*;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;

public class ScoreboardManagement {

    public static int howMainScore = 20;

    public static Scoreboard getMain() {
        Scoreboard scoreboard = Main.scoreboard.getNewScoreboard();

        Objective sidebar = scoreboard.registerNewObjective("sidebar", "dummy");

        sidebar.setDisplayName(Messages.masterpik);
        sidebar.setDisplaySlot(DisplaySlot.SIDEBAR);

        Score sidebarScore1 = sidebar.getScore(" ");
        sidebarScore1.setScore(howMainScore);

        Score sidebarScore2 = sidebar.getScore(Messages.main);
        sidebarScore2.setScore(howMainScore - 1);

        Score sidebarScore3 = sidebar.getScore("  ");
        sidebarScore3.setScore(howMainScore - 2);


        return scoreboard;
    }

    public static Scoreboard addTeams(Player player) {
        Scoreboard scoreboard = ScoreboardManagement.getMain();

        Objective sidebar = scoreboard.getObjective("sidebar");

        TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer(player);
        if (Tplayer != null) {

            Party party = Tplayer.getParty();

            ArrayList<com.masterpik.tower.party.Team> teams = new ArrayList<>();
            teams.addAll(party.getTeams().values());

            ArrayList<TowerPlayer> playersInTeam = new ArrayList<TowerPlayer>();

            int bucle = 0;
            int count = 3;
            int bucle2 = 0;

            Score teamsScore = sidebar.getScore(Messages.main);

            while (bucle < teams.size()) {

                bucle2 = 0;

                Team tim = scoreboard.registerNewTeam(TeamManagement.colorToString(teams.get(bucle).getColor()));
                playersInTeam.addAll(teams.get(bucle).getPlayers().values());
                tim.setAllowFriendlyFire(false);
                tim.setCanSeeFriendlyInvisibles(false);
                tim.setPrefix("" + TeamManagement.colorToChatString(teams.get(bucle).getColor()) + "§l");
                tim.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);

                while (bucle2 < playersInTeam.size()) {

                    tim.addPlayer(playersInTeam.get(bucle2).getPlayer());

                    bucle2++;
                }


                teamsScore = sidebar.getScore(ScoreboardManagement.getNameScore(teams.get(bucle).getColor(), player));
                teamsScore.setScore(howMainScore - count);
                count++;

                playersInTeam.clear();

                bucle++;
            }
        }


        return scoreboard;
    }

    public static String getNameScore(Byte colore,Player player) {
        String score = "";

        Scoreboard scoreboard = player.getScoreboard();

        Objective sidebar = scoreboard.getObjective("sidebar");
        String color = "§r";
        int points = -1;


        TowerPlayer Tplayer = PlayerManagement.playerToTowerPlayer(player);

        if (Tplayer != null) {

            Party party = Tplayer.getParty();


            color = "" + TeamManagement.colorToChatString(colore) + "§l";

            points = party.getTeams().get(colore).getPoint();

            String teamstr = "" + color + Messages.bloc1 + " " + TeamManagement.colorToString(colore);

            String prefix = points + "/" + Settings.pointToWin;

            String space = "";

            int bucle1 = 0;
            int maxscor = 18;

        /*Bukkit.getLogger().info("teamstr = "+Integer.toString(teamstr.length()));
        Bukkit.getLogger().info("prefix = "+Integer.toString(prefix.length()));
        Bukkit.getLogger().info("result = "+Integer.toString((23-(prefix.length())-(teamstr.length()))));*/
            while (bucle1 < (maxscor - (prefix.length()) - (teamstr.length()))) {

                space = space + " ";

                bucle1++;
            }

            String fix = TeamManagement.colorToFix(colore);

            score = teamstr + fix + space + prefix;
        }

        return score;
    }

    public static void refreshScoreboard(Party party) {
        ArrayList<TowerPlayer> playersParty = new ArrayList<>();
        playersParty.addAll(party.getPlayers().values());

        int bucle1 = 0;

        while (bucle1 < playersParty.size()) {

            playersParty.get(bucle1).getPlayer().setScoreboard(ScoreboardManagement.addTeams(playersParty.get(bucle1).getPlayer()));

            bucle1 ++;
        }
    }

}
